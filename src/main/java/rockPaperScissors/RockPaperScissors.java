package rockPaperScissors;

import java.util.Arrays;
import java.util.List;
import java.util.Random;
import java.util.Scanner;

public class RockPaperScissors {
	
	public static void main(String[] args) {
    	/* 	
    	 * The code here does two things:
    	 * It first creates a new RockPaperScissors -object with the
    	 * code `new RockPaperScissors()`. Then it calls the `run()`
    	 * method on the newly created object.
         */
        new RockPaperScissors().run();
    }
    
    
    Scanner sc = new Scanner(System.in);
    int roundCounter = 1;
    int humanScore = 0;
    int computerScore = 0;
    List<String> rpsChoices = Arrays.asList("rock", "paper", "scissors");
    
    public void run() {
        // TODO: Implement Rock Paper Scissors
    	String userInput = "y";
    	Random rand = new Random();
    	
    	while(!userInput.equalsIgnoreCase("n")) {
    		
    		// Generate new Random choice, pick from list with matching index
    		int roundChoice = rand.nextInt(3);
    		
    		System.out.println("Let's play round " +  roundCounter);
    		userInput = readInput("Your choice (Rock/Paper/Scissors)?");
    		
    		// While loop that continuously asks for a valid input (valid inputs are taken from rps list)
    		while(!rpsChoices.contains(userInput.toLowerCase())) {
    			System.out.println("I don't understand " + userInput + ". Try again");
    			userInput = readInput("Your choice (Rock/Paper/Scissors)?");
    		}
    		
    		//Calls method for declaring winner or tie, returns a string with the correlating text (also updates score)
    		String winnerText = declareWinner(rpsChoices.get(roundChoice), userInput.toLowerCase());
    		
    		System.out.println("Human chose " + userInput+ ", computer chose " + rpsChoices.get(roundChoice) +". " + winnerText);
    		System.out.println("Score: human " + humanScore + ", computer " + computerScore);
    		
    		// Copy of the code above for checking if the player wants to exit
    		while(!userInput.equalsIgnoreCase("n") && !userInput.equalsIgnoreCase("y")) {
    			userInput = readInput("Do you wish to continue playing? (y/n)?");
    			if(!userInput.equalsIgnoreCase("n") && !userInput.equalsIgnoreCase("y")) {
    				System.out.println("I don't understand " + userInput + ". Try again");
    			}
    		}
    		roundCounter++;
    	}
    	
    	System.out.println("Bye bye :)");
    	
    }

    /**
     * Reads input from console with given prompt
     * @param prompt
     * @return string input answer from user
     */
    public String readInput(String prompt) {
        System.out.println(prompt);
        String userInput = sc.next();
        return userInput;
    }
    
    public String declareWinner(String computer, String user) {
    	String endLine = "";
		
		if(computer.equalsIgnoreCase(user)) {
			endLine = "It's a tie";
		}else {
			String quickTest = computer + " " + user;
			
			if(quickTest.contains("rock") && quickTest.contains("paper")) {
				if(user.equalsIgnoreCase("paper")) {
					endLine = "Human wins.";
					humanScore++;
				}
				else {
					endLine = "Computer wins.";
					computerScore++;
				}
			}
			if(quickTest.contains("rock") && quickTest.contains("scissors")) {
				if(user.equalsIgnoreCase("rock")) {
					endLine = "Human wins.";
					humanScore++;
				}
				else {
					endLine = "Computer wins.";
					computerScore++;
				}
			}
			if(quickTest.contains("scissors") && quickTest.contains("paper")) {
				if(user.equalsIgnoreCase("scissors")) {
					endLine = "Human wins.";
					humanScore++;
				}
				else {
					endLine = "Computer wins.";
					computerScore++;
				}
			}
			
			
			
		}
    	
    	return endLine;
    }

}
